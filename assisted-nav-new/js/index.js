jQuery( document ).ready(function($) {

	var count = 1;
	$('.submit').hide();

    $('.owl-carousel').owlCarousel({
    	loop:false,
    	nav:false,
		dots: false,
		mouseDrag:false,
		items:1,
		autoHeight:true,
		animateOut: 'fadeOutDown',
		animateIn: 'fadeInUp'
	});
	
	$('.continue').click(function() {
		$('.owl-carousel').trigger('next.owl.carousel');
		count++;
		
		if(count === 2){
			$('.form-progress').animate({width:"66.6666%"});
			$('.step').html('2');
		}
		
		if(count === 3){
			$('.continue').hide();
			$('.submit').show();
			$('.step').html('3');
			$('.form-progress').animate({width:"100%"});
		}
		
	});
});